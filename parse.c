#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

int filecount = 0;
int sampleNum = 0;
char flag;
int firstfile = 0;
int laststamp = 0;
int badcount = 0;
char buffer[10];
char fileName[20] = "";
char realname[50] = "";
struct HsAccelReadings
{
   uint16_t accelX;
   uint16_t accelY;
   uint16_t accelZ;
}__attribute((packed));

typedef struct HsAccelReadings HsAccelReadings;

struct accelPacket {
   uint32_t timeStamp;
   struct {
      char data[8];
   } readings[40];
} __attribute((packed));

typedef struct accelPacket accelPacket;

static int packet_cnt = 0;

#define trace_ret() printf ("\n\r")
#define trace_print(foo) printf(foo)
#define trace_print_int(one, two) printf("0x%02X", one)

void printBinary(uint8_t foo) {
 trace_print_int(foo,2);
 trace_print(" = ");
 trace_print("0b");
 int i;
 for (i = 7; i >= 0; --i) {
    if (foo & (1 << i)) trace_print("1");
    else trace_print("0");
 }
 trace_ret();
}

void fillHsAccelReadings(HsAccelReadings * readings, uint32_t rawData)
{
   //as the ICD says throw away lower 2 bits
   rawData >>= 2;

   readings->accelX = (0x3ff & rawData);
   rawData >>= 10;
   readings->accelY = (0x3ff & rawData);
   rawData >>= 10;
   readings->accelZ = (0x3ff & rawData);
}

void print_packet(FILE* out, accelPacket* pkt, char flag)
{
    
   int i;
   
   //flip the bytes for the timestamp
   pkt->timeStamp = ntohl(pkt->timeStamp);

  sampleNum = 1;

if(flag == 't'){
     fprintf(out,"%d\n", pkt->timeStamp);
}
else {

for (i = 0; i < sizeof(pkt->readings) / sizeof(pkt->readings[0]); i++)
   {
      uint32_t *  rawAccelValues = (uint32_t *) &pkt->readings[i];
      HsAccelReadings reading1, reading2;

      //flip bytes from big endian to little endian 
      rawAccelValues[0] = ntohl(rawAccelValues[0]);
      rawAccelValues[1] = ntohl(rawAccelValues[1]);

      fillHsAccelReadings(&reading1, rawAccelValues[0]);
      fillHsAccelReadings(&reading2, rawAccelValues[1]);


    fprintf(out,"%d,%d,%i,%i,%i,%i,%i,%i\n", pkt->timeStamp, 
            (pkt->timeStamp + sampleNum++),
              reading1.accelX, reading1.accelY, reading1.accelZ,
              reading2.accelX, reading2.accelY, reading2.accelZ);
}
}

    


   

   //fprintf(out,"%d\n", pkt->timeStamp);
   packet_cnt++;
}

void print_packet_errors(FILE* out, accelPacket* pkt, char flag)
{

   int i;

   //flip the bytes for the timestamp
   pkt->timeStamp = ntohl(pkt->timeStamp);

  sampleNum = 1;
if((pkt->timeStamp-laststamp) > 50 | (pkt->timeStamp-laststamp) < 20)
{
	badcount++;
if(badcount >1 && badcount < 7)
{
printf("%d %d\n",pkt->timeStamp,(pkt->timeStamp-laststamp));
fprintf(out,"%d,%d\n", pkt->timeStamp,(pkt->timeStamp-laststamp));
}
}

laststamp = pkt->timeStamp;


   //fprintf(out,"%d\n", pkt->timeStamp);
   packet_cnt++;

}

int usage(const char *app)
{
   printf("Usage: %s <input file> <parse flag>\n", app);
   printf("Parse Flags:\nt = timetag only\ne = packet errors only\n");
   printf("e flag defaults to starting at DATA1, and only the numeric\n");
   printf("value of the filename should be entered\n-Must be sequential file numbers for batch processing");
   return 0;
}

int main(int argc, char *argv[], char flag)
{
	if (argc < 2)
      return usage(argv[0]);
    if (argc < 3)
      flag = 'r';
    else
    flag = argv[2][0];
    
    if(flag != 'e')
    {
	
   const char *fileName = NULL;
   char realname[50];

   accelPacket data;

   fileName = argv[1];
   strcpy(realname,argv[1]);
   strcat(realname,"_PARSED.csv");

   FILE* out = fopen(realname, "w");

   FILE* in = fopen(fileName, "r");

   if (!in) {
      printf("Failed to open input file %s\n", fileName);
      return 0;
   }

   int len, c;
    
    fprintf(out,"Count,X1,Y1,Z1,X2,Y2,Z2\n");
   do
   {
      c = fgetc(in);
      if (c == 'C') 
      {
         c = fgetc(in);
         if (c == 'P')
         {
            c = fgetc(in);
            if (c == 'S') 
            {
               c = fgetc(in);
               if (c == 'S')
               {
                  len = fread(&data, sizeof(accelPacket), 1, in);
                  if (len != 1) break;
                  print_packet(out, &data, flag);
               }
            }
         }
      }

      if (c == EOF) break;
      //len = fread(&data, sizeof(accelPacket), 1, in) 

      //print_packet(out, &data);

   } while(1);

   fclose(in);
   fclose(out);
      printf("%s\n", realname);
   printf("Read %d packets\n", packet_cnt);

   return 0;
}
else if (flag == 'e')
{
	FILE* out;


	   accelPacket data;

   filecount = atoi(argv[1]);
   firstfile = filecount;

  


while(1)
{
	
    strcpy(fileName,"DATA");
   sprintf(buffer, "%d", filecount);
   strcat(fileName, buffer);\
   if( access(fileName, F_OK ) != -1 ) {
    printf("%s\n",fileName);


	if(filecount == firstfile)
	out = fopen("HS_PARSED_PACKETANOMALY.csv","w");
	
   FILE* in = fopen(fileName, "r");

   if (!in) {
      printf("Failed to open input file %s\n", fileName);
      return 0;
   }

   int len, c;

    fprintf(out,"%s\n",fileName);
   do
   {
      c = fgetc(in);
      if (c == 'C')
      {
         c = fgetc(in);
         if (c == 'P')
         {
            c = fgetc(in);
            if (c == 'S')
            {
               c = fgetc(in);
               if (c == 'S')
               {
                  len = fread(&data, sizeof(accelPacket), 1, in);
                  if (len != 1) break;
                  print_packet_errors(out, &data, flag);
               }
            }
         }
      }

      if (c == EOF) break;
      //len = fread(&data, sizeof(accelPacket), 1, in)

      //print_packet(out, &data);

   } while(1);

   fclose(in);

}else {
    printf("%s Does not exist\n",fileName);
    return 0;
    }

	strcpy(fileName,"");
	strcpy(buffer, "");
    strcpy(realname, "");
    badcount = 0;
    laststamp = 0;
	filecount++;
	packet_cnt = 0;
}
       fclose(out);
	
}
}
